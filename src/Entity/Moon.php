<?php

namespace App\Entity;

use App\Repository\MoonRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MoonRepository::class)
 */
class Moon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Planet::class, inversedBy="moons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $planet;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 0,
     *     max = 360,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $angle = 0;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 1,
     *     max = 75,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $velocity = 1;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\ExpressionLanguageSyntax(
     *     allowedVariables={"left", "right"}
     * )
     */
    private $rotation;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 5,
     *     max = 10,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $orbital_radius = 5;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $active = true;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color = "#c4c4c4";

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlanet(): ?Planet
    {
        return $this->planet;
    }

    public function setPlanet(?Planet $planet): self
    {
        $this->planet = $planet;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAngle(): ?string
    {
        return $this->angle;
    }

    public function setAngle(string $angle): self
    {
        $this->angle = $angle;

        return $this;
    }

    public function getVelocity(): ?string
    {
        return $this->velocity;
    }

    public function setVelocity(string $velocity): self
    {
        $this->velocity = $velocity;

        return $this;
    }

    public function getRotation(): ?string
    {
        return $this->rotation;
    }

    public function setRotation(string $rotation): self
    {
        $this->rotation = $rotation;

        return $this;
    }

    public function getOrbitalRadius(): ?string
    {
        return $this->orbital_radius;
    }

    public function setOrbitalRadius(string $orbital_radius): self
    {
        $this->orbital_radius = $orbital_radius;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function populateFromArray(array $data): void
    {
        $this->setName($data['name']);
        $this->setAngle($data['angle']);
        $this->setVelocity($data['velocity']);
        $this->setRotation($data['rotation']);
        $this->setOrbitalRadius($data['orbital_radius']);
        $this->setActive(true);
        $this->setPlanet($data['planet']);
        $this->setColor($data['color']);
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
