export default class ModalHandler {

    static openModal() {
        const $modal = jQuery('.modal__disclaimer');
        if(!$.cookie("modal-done")) {
            $modal.addClass("active");
        }
    }

    static dismissModal() {
        $.cookie("modal-done", true);
        const $modal = $('.modal__disclaimer');
        $modal.removeClass("active");
    }
}