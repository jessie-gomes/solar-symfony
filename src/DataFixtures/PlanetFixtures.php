<?php

namespace App\DataFixtures;

use App\Entity\Planet;
use App\Entity\Star;
use App\Repository\StarRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PlanetFixtures extends Fixture implements OrderedFixtureInterface
{

    /**
     * @var StarRepository
     */
    private $starRepository;
    /**
     * @var null
     */
    private $manager;


    public function __construct(StarRepository $starRepository)
    {
        $this->manager = null;
        $this->starRepository = $starRepository;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->populatePlanets();
        $this->manager->flush();
    }

    public function getOrder(): int
    {
        return 4;
    }

    private function createPlanet(array $singlePlanetData): void
    {
        $planet = new Planet();
        $planet->populateFromArray($singlePlanetData);
        $this->manager->persist($planet);
    }

    private function populatePlanets()
    {
        $planetData = $this->getPlanetData();
        foreach ($planetData as $planet) {
            $this->createPlanet($planet);
        }
    }

    private function getStar(): Star
    {
        return $this->starRepository->findAll()[0];
    }

    private function getPlanetData(): array
    {
        $star = $this->getStar();

        return [
            "mercury" => [
                "star" => $star,
                "name" => "Mercury",
                "angle" => rand(0, 359),
                "velocity" => 48,
                "rotation" => "left",
                "diameter" => 250,
                "color" => "#f0d281",
                "orbital_radius" => 2300
            ],
            "venus" => [
                "star" => $star,
                "name" => "Venus",
                "angle" => rand(0, 359),
                "velocity" => 35,
                "rotation" => "left",
                "diameter" => 250,
                "color" => "#ccc3ab",
                "orbital_radius" => 4000
            ],
            "earth" => [
                "star" => $star,
                "name" => "Earth",
                "angle" => rand(0, 359),
                "velocity" => 30,
                "rotation" => "left",
                "diameter" => 250,
                "color" => "#5a8cc4",
                "orbital_radius" => 5000
            ],
            "mars" => [
                "star" => $star,
                "name" => "Mars",
                "angle" => rand(0, 359),
                "velocity" => 24,
                "rotation" => "left",
                "diameter" => 150,
                "color" => "#cf753a",
                "orbital_radius" => 6000
            ],
            "jupiter" => [
                "star" => $star,
                "name" => "Jupiter",
                "angle" => rand(0, 359),
                "velocity" => 13,
                "rotation" => "left",
                "diameter" => 750,
                "color" => "#c9af9d",
                "orbital_radius" => 10000
            ],
            "saturn" => [
                "star" => $star,
                "name" => "Saturn",
                "angle" => rand(0, 359),
                "velocity" => 10,
                "rotation" => "left",
                "diameter" => 650,
                "color" => "#d1c499",
                "orbital_radius" => 12000
            ],
            "uranus" => [
                "star" => $star,
                "name" => "Uranus",
                "angle" => rand(0, 359),
                "velocity" => 7,
                "rotation" => "left",
                "diameter" => 300,
                "color" => "#abced1",
                "orbital_radius" => 13000
            ],
            "neptune" => [
                "star" => $star,
                "name" => "Neptune",
                "angle" => rand(0, 359),
                "velocity" => 5,
                "rotation" => "left",
                "diameter" => 450,
                "color" => "#568ac4",
                "orbital_radius" => 13500
            ],
            "pluto" => [
                "star" => $star,
                "name" => "Pluto",
                "angle" => rand(0, 359),
                "velocity" => 4,
                "rotation" => "left",
                "diameter" => 50,
                "color" => "#dbb3a7",
                "orbital_radius" => 15000
            ]
        ];
    }
}