<?php

namespace App\EventSubscriber;

use App\Entity\Planet;
use App\Repository\StarRepository;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{

    /**
     * @var StarRepository
     */
    private StarRepository $starRepository;

    public function __construct(StarRepository $starRepository)
    {
        $this->starRepository = $starRepository;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['entityPersistedCallback']
        ];
    }

    public function entityPersistedCallback(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if ($entity instanceof Planet) {
            $this->injectStarToPlanet($entity);
            return;
        }
    }

    /**
     * Private Methods
     */
    private function injectStarToPlanet($planet)
    {
        $star = $this->starRepository->findOneBy([]);
        $planet->setStar($star);
    }
}