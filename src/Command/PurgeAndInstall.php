<?php

namespace App\Command;


use App\Entity\Star;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PurgeAndInstall extends Command
{
    protected static $defaultName = 'solar:install';
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    /**
     * @var UserPasswordHasherInterface
     */
    private $hasher;

    /**
     * @param UserPasswordHasherInterface $hasher
     * @param EntityManagerInterface $objectManager
     */
    public function __construct( UserPasswordHasherInterface $hasher, EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->hasher = $hasher;
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {

        $dropCommand = $this->getApplication()->find('doctrine:database:drop');
        $createCommand = $this->getApplication()->find('doctrine:database:create');
        $migrateCommand = $this->getApplication()->find('doctrine:migrations:migrate');
        $installCommand = $this->getApplication()->find('solar:setup');

        $argDrop = ["--force" => true];
        $inputDrop = new ArrayInput($argDrop);


        try {
            $dropCommand->run($inputDrop, $output);
        } catch(\Exception $exception) {
            $output->writeln("Database could not be dropped.");
        }

        try {
            $createCommand->run($input, $output);
        } catch(\Exception $exception) {
            $output->writeln("Database could not be created... Already exists?");
        }



        $argMigrate = ["--no-interaction" => true];
        $inputMigrate = new ArrayInput($argMigrate);
        $migrateCommand->run($inputMigrate, $output);


        $demoAdmin = new User();
        $demoAdmin->setUsername("demo");
        $demoAdmin->setRoles(["ROLE_ADMIN"]);
        $demoAdmin->setPassword($this->hashPassword($demoAdmin));

        $demoStar = new Star();
        $demoStar->setName("Sun");

        $this->objectManager->persist($demoStar);
        $this->objectManager->persist($demoAdmin);
        $this->objectManager->flush();

        $installCommand->run($inputMigrate, $output);

        return Command::SUCCESS;
    }

    private function hashPassword(User $user): string
    {
        $defaultPassword = "demo";
        return $this->hasher->hashPassword($user, $defaultPassword);
    }

}