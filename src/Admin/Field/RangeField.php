<?php

namespace App\Admin\Field;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class RangeField implements FieldInterface
{
    use FieldTrait;

    /**
     * @param string $propertyName
     * @param null $label
     * @param array $options
     * @return static
     */
    public static function new(string $propertyName, $label = null, array $options = []): self
    {
        $options['attr']['id'] = $propertyName;

        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(RangeType::class)
            ->addCssClass('field-angle')
            ->setFormTypeOptions($options)
            ;
    }
}