# Solar Symfony *v1*
>###### *orbit generator on symfony 5.3*
---

### Installation
>###### follow this steps to install it
---

1. clone | fork
1. run `composer install`.
1. run `yarn | npm install`.
1. run `npm run dev ` once to generate assets.
1. run `php bin/console solar:install` to start the required data.
1. run `php bin/console solar:demo` to populate demo data *(optional)* 
1. run `composer server:run | symfony serve` to start the symfony server
1. play the *galileo*
---
---
---