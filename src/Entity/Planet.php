<?php

namespace App\Entity;

use App\Repository\PlanetRepository;
use App\Service\OrbitalCalculations;
use App\Service\OrbitCalculationsRevamped;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectManagerAware;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PlanetRepository::class)
 */
class Planet //implements ObjectManagerAware
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Star::class, inversedBy="planets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $star;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 0,
     *     max = 360,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $angle = 0;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 1,
     *     max = 75,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $velocity = 1;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\ExpressionLanguageSyntax(
     *     allowedVariables={"left", "right"}
     * )
     */
    private $rotation;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 1000,
     *     max = 15000,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $orbital_radius = 1000;

    /**
     * @ORM\OneToMany(targetEntity=Moon::class, mappedBy="planet", orphanRemoval=true)
     */
    private $moons;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $active = true;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\Range(
     *     min = 200,
     *     max = 750,
     *     notInRangeMessage="You must pick between {{ min }} and {{ max }} as value."
     * )
     */
    private $diameter = 200;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color = "#a9fbfc";

    public function __construct()
    {
        $this->moons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStar(): ?Star
    {
        return $this->star;
    }

    public function setStar(?Star $star): self
    {
        $this->star = $star;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAngle(): ?string
    {
        return $this->angle;
    }

    public function setAngle(string $angle): self
    {
        $this->angle = $angle;

        return $this;
    }

    public function getVelocity(): ?string
    {
        return $this->velocity;
    }

    public function setVelocity(string $velocity): self
    {
        $this->velocity = $velocity;

        return $this;
    }

    public function getRotation(): ?string
    {
        return $this->rotation;
    }

    public function setRotation(string $rotation): self
    {
        $this->rotation = $rotation;

        return $this;
    }

    public function getOrbitalRadius(): ?string
    {
        return $this->orbital_radius;
    }

    public function setOrbitalRadius(string $orbital_radius): self
    {
        $this->orbital_radius = $orbital_radius;

        return $this;
    }

    /**
     * @return Collection|Moon[]
     */
    public function getMoons(): Collection
    {
        return $this->moons;
    }

    public function addMoon(Moon $moon): self
    {
        if (!$this->moons->contains($moon)) {
            $this->moons[] = $moon;
            $moon->setPlanet($this);
        }

        return $this;
    }

    public function removeMoon(Moon $moon): self
    {
        if ($this->moons->removeElement($moon)) {
            // set the owning side to null (unless already changed)
            if ($moon->getPlanet() === $this) {
                $moon->setPlanet(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function __toString()
    {
        return "Planet - " . $this->getName();
    }

    public function populateFromArray(array $data): void
    {
        $this->setName($data['name']);
        $this->setAngle($data['angle']);
        $this->setVelocity($data['velocity']);
        $this->setRotation($data['rotation']);
        $this->setOrbitalRadius($data['orbital_radius']);
        $this->setActive(true);
        $this->setStar($data['star']);
        $this->setDiameter($data['diameter']);
        $this->setColor($data['color']);
    }

    public function getOrbitTime(): float
    {
        $calculations = new OrbitalCalculations($this);
        return $calculations->getSecondsPerOrbit();
    }

    public function getDiameter(): ?string
    {
        return $this->diameter;
    }

    public function setDiameter(string $diameter): self
    {
        $this->diameter = $diameter;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
