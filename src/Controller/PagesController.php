<?php

namespace App\Controller;

use App\Repository\MoonRepository;
use App\Repository\PlanetRepository;
use App\Repository\StarRepository;
use App\Service\OrbitalRenderer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @var StarRepository
     */
    private $starRepository;
    /**
     * @var PlanetRepository
     */
    private $planetRepository;
    /**
     * @var MoonRepository
     */
    private $moonRepository;
    /**
     * @var OrbitalRenderer
     */
    private $orbitalRenderer;

    public function __construct(StarRepository $starRepository, PlanetRepository $planetRepository, MoonRepository $moonRepository)
    {
        $this->starRepository = $starRepository;
        $this->planetRepository = $planetRepository;
        $this->moonRepository = $moonRepository;
        $this->orbitalRenderer = new OrbitalRenderer($this->starRepository, $this->planetRepository, $this->moonRepository);
    }

    /**
     * @return Response
     * @Route("/", name="root")
     */
    public function index(): Response
    {
        $data = $this->orbitalRenderer->getDumpForRootView();
        if (!$data) {
            return $this->redirectToRoute("install");
        }
        return $this->render('pages/index.html.twig', $data);
    }

    /**
     * @return Response
     * @Route("/install", name="install")
     */
    public function error(): Response
    {
        return $this->render("pages/install.html.twig");
    }
}
